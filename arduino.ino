int led = 12;
void setup() {
// Open serial communications
Serial.begin(9600);
// send an intro:
Serial.println("0=off, 1=on");
Serial.println();
pinMode(led,OUTPUT);
}

void loop() {
// get any incoming bytes:
if (Serial.available() > 0) {
int input = Serial.read();
if (input == 48) { //received 0
digitalWrite(led,LOW);
}
else if (input == 49) { //received 1
digitalWrite(led,HIGH);
}
}
}
