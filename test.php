<?php
include "php_serial.class.php";
$msg = $argv[1]; // 0=off, 1=on
$serial = new phpSerial;
$serial->deviceSet("/dev/ttyUSB0");
$serial->confBaudRate(9600);
$serial->confParity("none");
$serial->confCharacterLength(8);
$serial->confStopBits(1);
$serial->deviceOpen();
if (intval($msg)==0) $serial->sendMessage(0); //turn the led off
else $serial->sendMessage(1); //turn the led on
$serial->deviceClose();
?>
